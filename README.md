# Farento Test
### Run developer environment:
```
  % make dev-up
```
##### WEB: http://localhost
##### MailHog: http://localhost:8025
##### PhpMyAdmin: http://localhost:8080 (root/root db_name)

### Stop developer environment:
```
  % make dev-down
```

### Run BASH:
```
    make dev-bash
```
### Run LOG`S tail:
```
    make dev-logs
```
### Run tests:
```
    make dev-test
```
