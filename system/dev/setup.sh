#!/bin/bash
composer install

while ! nc -z -v -w30 mysql-test 3306;
    do
        echo "Waiting database server"
        sleep 1;
done;

bin/console doctrine:database:create --if-not-exists  --no-interaction
bin/console doctrine:migrations:migrate --no-interaction
#bin/console doctrine:fixtures:load --no-interaction
service supervisor reread
service supervisor update
service supervisor start
service nginx start
exec "$@"