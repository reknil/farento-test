dev-command:
	docker-compose exec  app bin/console $(c)
dev-up:
	docker-compose  up --build -d
dev-down:
	docker-compose  down --remove-orphans
dev-logs:
	docker-compose  logs -f
dev-test:
	docker-compose  exec app php ./vendor/bin/simple-phpunit
dev-bash:
	docker-compose  exec app bash