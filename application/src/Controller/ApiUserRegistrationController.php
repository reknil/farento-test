<?php

namespace App\Controller;

use App\Form\UserRegistrationType;
use App\Service\UserRegistrationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ApiUserRegistrationController extends AbstractController
{
    /**
     * @Route("/api/user/registration", name="api_user_registration", methods={"POST"})
     * @param Request $request
     * @param UserRegistrationService $userRegistrationService
     * @return Response
     */
    public function index(Request $request, UserRegistrationService $userRegistrationService)
    {
        try {
            $user = $userRegistrationService->registrationByApi($request);
        } catch (\Exception $exception){
            throw new BadRequestHttpException($exception->getMessage());
        }
        return $this->json([
            'username' => $user->getUsername(),
            'name' => $user->getName(),
            'message' => 'Check email',
        ]);
    }
}
