<?php

namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserRegistrationService
{

    private $validator;
    private $em;
    private $passwordEncoder;
    private $generatedPassword;
    private $mailer;

    /**
     * UserRegistrationService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailerInterface $mailer
     */
    public function __construct(
        ValidatorInterface $validator,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        MailerInterface $mailer
    ) {
        $this->validator = $validator;
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->generatedPassword = $this->passwordGenerate();
        $this->mailer = $mailer;
    }

    /**
     * @return string
     */
    public function getGeneratedPassword(): string
    {
        return $this->generatedPassword;
    }

    /**
     * @param Request $request
     * @return User
     */
    public function registrationByApi(Request $request)
    {

        $user = new User($request->get('email'), $request->get('name'));
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new BadRequestHttpException((string)$errors, null, 418);
        }
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $this->generatedPassword)
        );
        $this->em->persist($user);
        $this->em->flush();
        $this->sendRegistrationEmail($user);
        return $user;
    }

    /**
     * @param Request $request
     */
    public function registrationBySomeService(Request $request)
    {
        //TODO
        // В будущем можно разделить на отдельные классы
    }

    ################################

    private function sendRegistrationEmail(User $user)
    {
        $email = (new Email())
            ->from(new Address('info@exam.com', 'Sender Name'))
            ->to(new Address($user->getEmail(), $user->getName()))
            ->subject('Please Confirm your Email')
            ->text($this->generatedPassword);
        return $this->mailer->send($email);
    }

    private function passwordGenerate()
    {
        $generator = new RequirementPasswordGenerator();

        $generator
            ->setLength(16)
            ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, true)
            ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, true)
            ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, true);
        return $generator->generatePassword();
    }

}