<?php

namespace App\Tests\Controller;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiUserRegistrationControllerTest extends WebTestCase
{

    /**
     * @var KernelBrowser|
     */
    private $client = null;
    /**
     * @var EntityManager
     */
    private $entityManager = null;

    protected function setUp()
    {
        parent::setUp();
        self::ensureKernelShutdown();
        if (null === $this->client) {
            $this->client = static::createClient();
        }
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy([
            'email' => 'email@email.com'
        ]);
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        parent::tearDown();
    }


    public function testUGenerator()
    {
        $this->client->request('POST', 'api/user/registration', [
            'name' => 'Test',
            'email' => 'email@email.com'
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    }
}
