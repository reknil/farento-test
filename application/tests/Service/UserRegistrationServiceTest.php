<?php

namespace App\Tests\Service;

use App\Service\UserRegistrationService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRegistrationServiceTest extends WebTestCase
{


    /**
     * @var UserRegistrationService
     */
    private $userRegistrationService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        self::bootKernel();
        $this->userRegistrationService = (self::$container)->get(UserRegistrationService::class);
    }

    public function testPasswordGenerator()
    {
        $pass = $this->userRegistrationService->getGeneratedPassword();
        $this->assertEquals(16, strlen($pass));
    }

}
